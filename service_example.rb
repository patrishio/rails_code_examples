class UsesStratus
  # ------------------------------
  # INITIALIZE
  def initialize(main_address_id)
    @company = MainAddress.find_by_id(main_address_id)
  end

  # ------------------------------
  # GET GREET
  def get_greet    
    uri = URI('https://api.emea.fedex.com/stratus/v1/greet')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Bearer #{@company.stratus_token}"
    request['Content-Type'] = "application/json"
    response = http.request(request)
    response
  end

  # ------------------------------
  # POST GREET  
  def post_token    
    uri = URI('https://api.emea.fedex.com/auth/oauth/v2/token?grant_type=client_credentials&scope=oob')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Basic ZWQxYmY4OGYtNzJiNy00MDRiLWE2NmUtYWVlYTRlZmVjZTM2OmQ5NjVjNTdhLTllYTYtNDA3Mi04MjdlLTNjNzE0NTE4NmM1Mg=="
    request['Content-Type'] = "application/json"
    response = http.request(request)
    response
  end

  # ------------------------------
  # PARSE RESPONES
  def parse_response(response)    
    # utf_8_response = response.force_encoding('UTF-8')
    # obj = JSON.parse(utf_8_response)
    obj = JSON.parse(response)
  end
  
  # ------------------------------
  # SAVE TOKEN
  def save_token(response)                  
    pr = parse_response(response)    
    token = pr['access_token']
    @company.stratus_token = token
    @company.save!
  end

  # ------------------------------
  # CHECK IF TOKEN IS VALID
  def check_token
    response = get_greet
    if response.code == "200"
      return "Ok"
    else
      return response.code
    end
  end

  # ------------------------------
  # GET NEW TOKEN IN NECCESERY
  def ready_token       
   if check_token == "Ok"
   elsif check_token == "401"    
    save_token(post_token.body)
   end
    check_token
  end

  # ------------------------------
  # SEND SHIPMENT REQUEST
  def send_shipment_request(json_request)    
    uri = URI('https://api.emea.fedex.com/stratus/v1/shipments')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Post.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Bearer #{@company.stratus_token}"
    request['Content-Type'] = "application/json"
    request.body = json_request
    response = http.request(request)
    response    
  end

  # ------------------------------
  # GET SHIPMENT JUST
  def get_shipment(shipment_id)
    uri = URI('https://api.emea.fedex.com/stratus/v1/order/' + shipment_id)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Bearer #{@company.stratus_token}"
    request['Content-Type'] = "application/json"
    response = http.request(request)
    response 
  end

  # ------------------------------
  # GET SHIPMENT DETAILS
  def get_shipment_details(response)
    presponse = parse_response(response.body)        
    response_2 = get_shipment(presponse["Id"])
  end

  # ------------------------------
  # GET SHIPMENT LIST
  def get_shipment_list
    uri = URI('https://api.emea.fedex.com/stratus/v1/shipments')
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Bearer #{@company.stratus_token}"
    request['Content-Type'] = "application/json"
    response = http.request(request)
    response     
  end

  # ------------------------------
  # CANCEL SHIPMENT
  def cancel_shipment(transaction_id)
    uri = URI('https://api.emea.fedex.com/stratus/v1/transactions/' + transaction_id)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Delete.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Bearer #{@company.stratus_token}"    
    request['Content-Type'] = "application/json"
    response = http.request(request)
    response        
  end

  # ------------------------------
  # GET PACKAGES
  def get_packages(transaction_id)
    uri = URI('https://api.emea.fedex.com/stratus/v1/transactions/'+ transaction_id + '/packages')    
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Bearer #{@company.stratus_token}"
    request['Content-Type'] = "application/json"
    response = http.request(request)
    response         
  end

  # ------------------------------
  # GET LABELS
  def get_labels(transaction_id)
    uri = URI('https://api.emea.fedex.com/stratus/v1/transactions/'+ transaction_id + '/labels')    
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Bearer #{@company.stratus_token}"
    request['Content-Type'] = "application/json"
    response = http.request(request)
    response         
  end  

  # ------------------------------
  # GET LABEL DATA
  def get_label_data(transaction_id, label_id)
    uri = URI('https://api.emea.fedex.com/stratus/v1/transactions/'+ transaction_id + '/labels/' + label_id)        
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = true
    request = Net::HTTP::Get.new(uri.request_uri)
    request['x-api-key'] = @company.fedex_api_key
    request['Authorization'] = "Bearer #{@company.stratus_token}"
    request['Content-Type'] = "application/json"
    response = http.request(request)
    response         
  end    
  

end