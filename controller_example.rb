class ContactsController < ApplicationController
  
  before_action :set_contact, only: [:show, :edit, :update, :destroy]
  before_action :office
  #--------------------------
  def index
    @contacts = Contact.order(:name)
                       .search(params[:search_contact])
                       .paginate(page: params[:page], :per_page => 15)
                       .where(main_address_id: current_user.main_address_id)
    if params[:rep_filter].present? && params[:rep_filter] == "true"      
        @contacts = @contacts.where(rep: true)
    end
  end

  #--------------------------
  def new
    @contact = Contact.new    
  end

#--------------------------
  def create
    @contact = Contact.new(contact_params)
    @contact.main_address_id = current_user.main_address_id
    respond_to do |format|
      if @contact.save
        flash[:info] = 'contact was successfully created.'
        format.html { redirect_to contacts_url}
        format.json { render :index, status: :created, location: @contact }
      else
        format.html { render :new }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end    
  end

  #--------------------------
  def edit    
  end

  #--------------------------
  def update    
    respond_to do |format|
      if @contact.update(contact_params)
        flash[:info] = 'contact was successfully updated.'
        format.html { redirect_to contacts_url }
        format.json { render :index, status: :ok, location: @contact }
      else
        format.html { render :edit }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end    
  end

  #--------------------------
  def destroy
    @contact.destroy
    respond_to do |format|
      flash[:info] = 'Contact was successfully deleted.'
      format.html { redirect_to contacts_url }
      format.json { head :no_content }
    end
  end

  #--------------------------
  def import
    Contact.import(params[:file])
    redirect_to root_url, notice: "Contacts imported"
  end 
  


  private
  
  #--------------------------
  def set_contact
    @contact = Contact.find(params[:id])
  end


  #--------------------------
  def contact_params
      params.require(:contact).permit(:id, :name, :job_title, :tel_no, :fax_no, :mobile_no, 
                                      :email, :invoice_email, :contact_notes, :rep)
  end
end
